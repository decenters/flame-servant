{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses    #-}
{-# LANGUAGE OverloadedStrings        #-}
{-# LANGUAGE ScopedTypeVariables      #-}
{-# LANGUAGE TypeFamilies             #-}
{-# LANGUAGE TypeOperators            #-}
{-# LANGUAGE TypeApplications         #-}
{-# LANGUAGE UndecidableInstances     #-}
{-# LANGUAGE UndecidableSuperClasses  #-}
{-# LANGUAGE StandaloneDeriving       #-}
{-# LANGUAGE RankNTypes               #-}
{-# LANGUAGE OverloadedStrings        #-}
{-# LANGUAGE DeriveGeneric            #-}
{-# LANGUAGE PostfixOperators         #-}
{-# LANGUAGE ConstraintKinds          #-}
{-# LANGUAGE FunctionalDependencies   #-}
{-# LANGUAGE GADTs #-}
{-# OPTIONS_GHC -fplugin Flame.Solver #-}

module Flame.Servant.BasicAuth where
import Data.Constraint

import GHC.TypeLits                     (KnownNat, natVal, Symbol, KnownSymbol, symbolVal)
import Network.Wai                      (Request, requestHeaders, Response)

import Servant 
import Servant.Server.Internal
import Servant.Server.Internal.BasicAuth

import qualified Data.ByteString            as BS
import qualified Data.ByteString.Char8      as BC8
import           Data.Typeable
import           GHC.Generics hiding (C)

import Flame.Servant
import Flame.Principals
import Flame.Runtime.Principals
import Flame.Runtime.IO
import Flame.IFC
import Flame.TCB.IFC (unsafeUnlabel)
import Debug.Trace

import qualified Control.Monad.IO.Class as MIO (liftIO) 

type BasicAuthFLA (realm :: Symbol) (userData :: *) = BasicAuthIFC FLACT Lbl realm userData
type BasicAuthNM (realm :: Symbol) (userData :: *) = BasicAuthIFC NMT Lbl realm userData

data BasicAuthIFC  (m :: (* -> *) -> (KPrin -> * -> *) -> KPrin -> KPrin -> * -> *)
                   (n :: (KPrin -> * -> *))
                   (realm :: Symbol)
                   (userData :: *)
  deriving (Typeable)

-- | Basic Authentication
instance ( KnownSymbol realm
         , HasServer api context
         , HasContextEntry context (IFCAuthCheck m n (BasicAuthIFC m n realm usr) usr)
         , IFC m IO n
         , IFCApp (BasicAuthIFC m n realm usr)
         )
    => HasServer (BasicAuthIFC m n realm usr :> api) context where

  type ServerT (BasicAuthIFC m n realm usr :> api) m' = usr -> ServerT api m'

  route Proxy context subserver =
    route (Proxy :: Proxy api) context (subserver `addAuthCheck` authCheck)
    where
       realm = BC8.pack $ symbolVal (Proxy :: Proxy realm)
       basicAuthContext = getContextEntry context
       authCheck = withRequest $ \ req -> runBasicAuthIFC @m @n (Proxy :: Proxy (BasicAuthIFC m n realm usr)) req realm basicAuthContext

type FLAuthCheck api usr = (Lbl (Client api) BasicAuthData) -> FLACT IO Lbl (I (AppServer api)) (I (AppServer api)) (BasicAuthResult usr)
type NMAuthCheck api usr = (Lbl (Client api) BasicAuthData) -> NMT IO Lbl (I (AppServer api)) (I (AppServer api)) (BasicAuthResult usr)

type IFCAuthCheck (m :: (* -> *) -> (KPrin -> * -> *) -> KPrin -> KPrin -> * -> *)
                  (n :: (KPrin -> * -> *)) api usr = (Lbl (Client api) BasicAuthData) -> m IO n (I (AppServer api)) (I (AppServer api)) (BasicAuthResult usr)

-- | Run and check basic authentication, returning the appropriate http error per
-- the spec.
runBasicAuthIFC :: forall m n api usr . (IFC m IO n, IFCApp api) => Proxy api -> Request -> BS.ByteString -> IFCAuthCheck m n api usr -> DelayedIO usr
runBasicAuthIFC api req realm check =
  case decodeBAHdr req of
     Nothing -> plzAuthenticate
     Just e  -> (MIO.liftIO $ runIFC $ check $ label e) >>= \res ->
                 case unsafeUnlabel res of
                  BadPassword    -> plzAuthenticate
                  NoSuchUser     -> plzAuthenticate
                  Unauthorized   -> delayedFailFatal err403
                  Authorized usr -> return usr
  where plzAuthenticate = delayedFailFatal err401 { errHeaders = [mkBAChallengerHdr realm] }