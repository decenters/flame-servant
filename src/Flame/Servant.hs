{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses    #-}
{-# LANGUAGE OverloadedStrings        #-}
{-# LANGUAGE ScopedTypeVariables      #-}
{-# LANGUAGE TypeFamilies             #-}
{-# LANGUAGE TypeOperators            #-}
{-# LANGUAGE TypeApplications         #-}
{-# LANGUAGE StandaloneDeriving       #-}
{-# LANGUAGE RankNTypes               #-}
{-# LANGUAGE OverloadedStrings        #-}
{-# LANGUAGE DeriveGeneric            #-}
{-# LANGUAGE PostfixOperators         #-}
{-# LANGUAGE ConstraintKinds          #-}
{-# LANGUAGE FunctionalDependencies   #-}
{-# LANGUAGE GADTs #-}
{-# OPTIONS_GHC -fplugin Flame.Solver #-}

module Flame.Servant
  (EnforceFLA, EnforceIFC, EnforceNM, IFCAuth, authorize, IFCApp(..))
where
import Data.Constraint

import GHC.TypeLits                     (KnownNat, natVal)
import Network.HTTP.Types.Status
import Network.HTTP.Types.Method
import Network.HTTP.Types.Header
import Network.Wai                      (Request, requestHeaders, Response)

import Control.Monad.Trans.Resource     (MonadResource (..), ResourceT, runResourceT)
import Data.Maybe                       (fromMaybe)

import Servant 
import Servant.Common.Req
import Servant.Client hiding (Client)
import qualified Servant.Client as C
import Servant.API.ContentTypes
import Servant.Server.Internal
import Servant.Foreign

import Flame.Principals
import Flame.Runtime.Principals
import Flame.Runtime.IO
import Flame.IFC
import Flame.TCB.IFC (unsafeUnlabel)
import Flame.TCB.Assume

import qualified Control.Monad.IO.Class as MIO (liftIO) 

type EnforceFLA (pc::KPrin) (l::KPrin) a = EnforceIFC FLACT Lbl pc l a
type EnforceNM (pc::KPrin) (l::KPrin) a = EnforceIFC NMT Lbl pc l a
newtype EnforceIFC (m :: (* -> *) -> (KPrin -> * -> *) -> KPrin -> KPrin -> * -> *)
                   (n :: (KPrin -> * -> *))
                   (pc::KPrin) (l::KPrin) a = EnforceIFC a

data IFCAuth api where
   IFCAuthorized :: IFCApp api =>
                 DPrin client
              -> (AppServer api :≽: (client ∧ (∇) client)
                 , Client api :===: client)
              -> IFCAuth api

authorize :: (IFCApp api, AppServer api ≽ (client ∧ (∇) client), Client api === client) =>
             DPrin client
          -> IFCAuth api
authorize client = IFCAuthorized client (ActsFor, Equiv)

class IFCApp api where

  type AppServer api :: KPrin
  type Client    api :: KPrin
  type AuthBound api :: KPrin

  -- TODO: how to ensure api is sane for choice of AppServer and Client?

  appServerPrin     :: Proxy api -> Prin
  currentClientPrin :: Proxy api -> Prin
  authBoundPrin     :: Proxy api -> Prin
  appServerTy       :: Proxy api -> SPrin (AppServer api)
  currentClientTy   :: Proxy api -> SPrin (Client api)
  authBoundTy       :: Proxy api -> SPrin (AuthBound api)

  appServer :: Proxy api -> DPrin (AppServer api)
  appServer api =  (appServerPrin api) <=> (appServerTy api)
  currentClient :: Proxy api -> DPrin (Client api)
  currentClient api =  (currentClientPrin api) <=> (currentClientTy api)
  authBound :: Proxy api -> DPrin (AuthBound api)
  authBound api =  (authBoundPrin api) <=> (authBoundTy api)

  mkHandler :: forall m n pc l a. (IFC m IO n, IFC m Handler n, AppServer api ≽ (C (Client api) ∧ I (Client api) ∧ (∇) (Client api))
                              , AuthBound api ⊑ pc
                              , AuthBound api ⊑ l
                              , (I (AppServer api)) ≽ I pc
                              , (I (AppServer api)) ≽ I l
                              , (I (Client api) ∧ C (AppServer api)) ≽ pc
                              , (C (Client api) ∧ I (AppServer api)) ≽ l
                              , pc ⊑ l
                              ) =>
            Proxy api
            -> m IO n (AuthBound api) (AuthBound api) (IFCAuth api)
            -> (forall client. DPrin client -> (Prin, Prin))
            -> DPrin pc
            -> DPrin l
            -> (forall client pc' l'.
                ((AppServer api) ≽ (client ∧ (∇) client), (Client api) === client) =>
                     DPrin client
                  -> DPrin pc' 
                  -> DPrin l'
                  -> Maybe ( pc :===: pc'
                           , l :===: l'
                           , m IO n pc' l' a
                           )
               )
            -> m Handler n pc l a
  mkHandler api auth_ toPCL pc l f = 
    liftIO $ use (reprotect auth_ :: m IO n pc l (IFCAuth api)) $ \auth ->
     case auth of
      IFCAuthorized (client :: DPrin client) (ActsFor, Equiv) ->
       let (pc'_, l'_) = toPCL client in
         withPrin pc'_ $ \(pc' :: DPrin pc') -> 
          withPrin l'_ $ \(l' :: DPrin l') -> 
           case f client pc' l' of
             Just (Equiv, Equiv, h) -> reprotect h
             _ -> error "could not make handler"
    where
      apiClient = currentClient api
      apiAppServer = appServer api
